import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np

class HistogramNormalize(colors.Normalize):
    def __init__(self, data, vmin=None, vmax=None):
        if vmin is not None:
            data = data[data > vmin]
        if vmax is not None:
            data = data[data < vmax]
            
        sorted_data = np.sort(data.flatten())
        self.sorted_data = sorted_data[np.isfinite(sorted_data)]
        colors.Normalize.__init__(self, vmin, vmax)

    def __call__(self, value, clip=None):
        return np.ma.masked_array(np.searchsorted(self.sorted_data, value) /
                                  len(self.sorted_data))

data = np.linspace(-0.01, 1e-6, 100**2).reshape(100, -1)
normalizer = HistogramNormalize(data)
fig, ax = plt.subplots()

cax = ax.imshow(data, norm=normalizer)
cbar = fig.colorbar(cax)
plt.show()

# For midpoint normalization use `colors.DivergingNorm`.
