# A collection of tutorials and potentially reusable code snippets.

Add links to the code with short descriptions below.

## [Set-up automatic syncing between your computer and `io`](syncthing.md)


## [Set-up ssh to `io`](ssh-setup.md)


## `ipynb`s with git: automatically remove output
Works on macOS and Linux, copy paste this whole command into a terminal (at once):
```
mkdir -p ~/.config && mkdir -p ~/.config/git
echo '*.ipynb filter=nbstripout' > ~/.config/git/attributes
cat > ~/.config/git/config << EOF
[filter "nbstripout"]
    clean = jupyter-nbconvert --stdin --ClearOutputPreprocessor.enabled=True --to notebook --stdout
    smudge = cat
    required = true
EOF
```

## [Handle data in your git repo with submodules](data-git-submodule.md)


## [Git Snippets](git.md)
Useful Git commands that serve as a summary of the excellent [Git Book](https://git-scm.com/book/en/v2/)

## [ArXiv feed watcher](arxivfv2.py)
Keep track of new papers on the arXiv without having to look at all the new abstracts every day. It fetches the RSS feed, runs a simple search for keywords in the title/abstract and authors and sends an email with the abstracts that match any of the criteria. It uses `feedparser`, which is on `io`. I run it with cron at 03:00 Monday to Friday. To set it up you need to fill out the search criteria and the SMTP email options so it can send an email from user1@server1.com to user2@server2.com.

## [Sparse diagonalization with MUMPS](mumps_sparse_diag.py)
Example on using `mumps` with `scipy.sparse.linalg.eigsh`.

## [Vagrant](vagrant/readme.md)
Vagrant is simple wrapper around VirtualBox that allows to easily setup configured
virtual machines. See the basic usage commands and a few examples.

[Centos](vagrant/examples/centos/Vagrantfile) could be probably very useful for admins
to see how things work on Centos7 system in an easy way.

## [Set-up your connection to hpc05 and explain usage](https://gitlab.kwant-project.org/qt/cookbook/tree/master/hpc05)

## [hpc05 usage checker](hpc05-usage.py)
See how many cores are currently in total and per user. Add an `alias` to your `~/.bash_profile` for easy usage:
```
alias stat="python /home/$USER/full_path_to_script"
```
<img width="274" alt="screen shot 2016-10-12 at 13 02 19" src="https://cloud.githubusercontent.com/assets/6897215/19307824/e4980ba0-907c-11e6-88e4-dca1948d957b.png">

## [colormap normalizers](cmap-normalizers.py)
Nomalize your colorscale by pinning the middle of the colormap with `MidpointNormalize` or by binning the data such in different parts of the colormap such that every range of data points is represented in the plot with `HistogramNormalize`.

## Spyview
Did an experimentalist sent you a `.mtx` Spyview file and you don't run Windows? Do not despair:
```
def open_mtx(fname):
    with open(fname, 'rb') as f:
        line1 = f.readline()
        *shape, entry_size = [int(i) for i in  f.readline().split()]
        data = np.fromfile(f, (np.float64 if entry_size == 8 else np.float32))
        data = data.reshape(shape).squeeze()

    keys = ['units', 'plot_settings', 'xname', 'xmin', 'xmax',
            'yname', 'ymin', 'ymax', 'zname', 'zmin', 'zmax']
    values = line1.decode("utf-8").replace('\n', '').split(',')
    meta_data = dict(zip(keys, values))
    return meta_data, data
```

## [What is axis orientation when doing 2D plots](http://nbviewer.jupyter.org/urls/gitlab.kwant-project.org/qt/cookbook/raw/master/axis_2d.ipynb)


## [Insallation of kwant from source into conda env](kwant-from-source.md)

## [WIP: xArray + HoloViews: Finding and ultimte way for gridded data](xarray_holoviews_gridded.ipynb)

## [LCAO Hamiltonian generator](lcao.py)

Tight-binding Hamiltonians are often given in the LCAO (linear combination of atomic orbitals) formalism.
You may have encountered large tables of parameters containing entries like $V_{pp\pi}$.
This utility computes the corresponding hopping matrices automatically, for example `lcao_term(1, 1, 1, [0, 0, 1])` will return $H_{pp\pi}$ for a bond in the `[0, 0, 1]` direction.
See docstring in [lcao.py](lcao.py) for more details.

## [Guide for uploading Gitlab CI artifacts to Dropbox](artifacts_and_dropbox.md)

## `owncloud` / `nextcloud` in your CLI

You may use it, for example, to perform real-time visualisations of your running
hpc jobs. You will be able to track the progress without the need to vpn/login
to the cluster. This example is for [surfdrive](https://surfdrive.surf.nl).

First, create a new `webdav` password in your
[settings page](https://surfdrive.surf.nl/files/index.php/settings/personal?sectionid=security).

Then, upload any file with `curl -u "user-id@tudelft.nl:webdav-pass" -X PUT "https://surfdrive.surf.nl/files/remote.php/nonshib-webdav/path/to/folder/$1" --data-binary @"$1"`.
You have to replace `user-id`, `webdav-pass`, `$1` with your user name, the
generated password and the file name, respectively.

- The file will appear as an ordinary file in your cloud. You may, for example,
share it via link: new uploads under the same name will keep sharing settings.
- Uploaded images can be displayed directly in the browser (and shared with your
colleagues). Sometimes, browsers prefer to not download updated image previews:
this might depend on OC/NC server settings.
- Make sure to keep your `webdav` password in secret. If you prepare a script
please make sure to set `chmod 700 your-script` as a minimal measure.
- similarly, you may download and remove files, create folders in your cloud by
replacing `PUT` with a proper `http` directive.

## Voice notifications in executable cells

If you’re running long simulations and want some sound notification when they’ve finished, [someone worked out how to do it](https://gist.github.com/parente/41a13f4c8fa6165d345cc7703be291a3). All you need is one handy function:
```
def speak(text, voice=0):
    '''
    You can select a voice by putting in an integer. If you overshoot the length of the voice list, the default voice (0) is used.
    '''
    from IPython.display import Javascript as js, clear_output
    # Escape single quotes
    text = text.replace("'", r"\'")
    display(
        js(
            f"""
    if(window.speechSynthesis) {{
        var synth = window.speechSynthesis;
        var voices = synth.getVoices();
        var utterThis = new window.SpeechSynthesisUtterance('{text}');
        utterThis.voice = voices[{voice}];
        synth.speak(utterThis);
    }}
    """
        )
    )
    # Clear the JS so that the notebook doesn't speak again when reopened/refreshed
    clear_output(False)

```

