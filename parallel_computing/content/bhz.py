# Copyright Rafal Skolasinski, available under simplified BSD license.

# Version taken from https://gitlab.com/meso/paper-magnetic-qsh/blob/dfb90b9a789c431f633b196cbd9abe85508abab1/code/bhz.py

import numpy as np
import tinyarray
import kwant

import scipy.linalg as la
import scipy.sparse.linalg as sla
from scipy.constants import physical_constants

from collections import namedtuple

try:
    from functools import lru_cache
except ImportError:
    from functools32 import lru_cache


# *********** namedtuples definitions **********
nt_bhz = namedtuple('nt_bhz', 'A B C D M g_par Delta')
nt_sim = namedtuple('nt_sim', 'L W V a Bx By dis salt mu_lead mu')


# ############# default parameters ##############
def define_parameters(L=None, W=None, V=0, a=4.0, Bx=0, By=0,
                      dis=0, salt='', mu_lead=0.0, mu=0.0):
    return nt_sim(L, W, V, a, Bx, By, dis, salt, mu_lead, mu)


# *********** Constants ************************
mu_b = physical_constants['Bohr magneton in eV/T'][0]
bhz = nt_bhz(A=.3645, B=-.686, D=-.512, M=-.01, Delta=.0016, C=0, g_par=-20.5)


# *********** Pauli and M Matrcies *******************
sigma0 = tinyarray.array([[1, 0], [0, 1]])
sigmax = tinyarray.array([[0, 1], [1, 0]])
sigmay = tinyarray.array([[0, -1j], [1j, 0]])
sigmaz = tinyarray.array([[1, 0], [0, -1]])

Mx = np.kron(sigmax, np.diag([1, 0])) * bhz.g_par
My = np.kron(sigmay, np.diag([1, 0])) * bhz.g_par


# *********** onsite nad hoppings definitions *******************
def bhz_onsite(a):
    A, B, D, M, C = bhz.A / 2 / a, bhz.B / a**2, bhz.D / a**2, bhz.M, bhz.C
    Delta = bhz.Delta
    return C * np.eye(4) + (M - 4 * B) * np.kron(sigma0, sigmaz) \
            - 4 * D * np.kron(sigma0, sigma0) \
            + np.kron(sigmay, sigmay) * Delta


def bhz_hopx(a):
    A, B, D, M, C = bhz.A / 2 / a, bhz.B / a**2, bhz.D / a**2, bhz.M, bhz.C
    return B * np.kron(sigma0, sigmaz) + D * np.kron(sigma0, sigma0) \
            + 1j * A * np.kron(sigmaz, sigmax)


def bhz_hopy(a):
    A, B, D, M, C = bhz.A / 2 / a, bhz.B / a**2, bhz.D / a**2, bhz.M, bhz.C
    return B * np.kron(sigma0, sigmaz) + D * np.kron(sigma0, sigma0) \
            - 1j * A * np.kron(sigma0, sigmay)


# *******************2D DISPERSION**************************************
def dispersion2D(Bx, By, a, momenta=np.linspace(-np.pi/2, np.pi/2, 101)):
    """Return dispersion of 2D BHZ model"""
    H0 = bhz_onsite(a) + mu_b * (Mx * Bx + My * By)
    H1, H2 = bhz_hopx(a), bhz_hopy(a)

    hc = lambda A: A.transpose().conjugate()
    ph = lambda k: np.exp(-1j*a*k)

    def diag(kx, ky):
        mat = H0 + H1*ph(kx) + hc(H1)*ph(-kx) + H2*ph(ky) + hc(H2)*ph(-ky)
        ev = la.eigh(mat, eigvals_only=True)
        return ev

    disp10 = np.array([diag(k, 0) for k in momenta])
    disp11 = np.array([diag(k/np.sqrt(2), k/np.sqrt(2)) for k in momenta])

    return momenta, disp10, disp11


# *************** EDGE POTENTIAL DEFINITION **********************
def potential(y, W, V):
    if y==0 or y==W-1:
        return V
    else:
        return 0


# ************************ System Definitions **************************
def pot_ons(site, par):
    """par is supposed to be namedtuple of type nt_sim defined above"""
    dis_func = kwant.digest.uniform
    x, y = site.pos

    rand = dis_func(str(site.tag), par.salt) - 0.5
    pot = np.eye(4) * potential(y, par.W, par.V)
    dis = np.eye(4) * par.dis * rand
    mag = mu_b * (Mx * par.Bx + My * par.By)
    return pot + dis + mag


def make_lead(W, a, finalized=True):
    """Make lead for bhz simulation."""
    def lead_shape(pos):
        (x, y) = pos
        return (0 <= y < W)

    lat = kwant.lattice.square()
    lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))

    ons = bhz_onsite(a)
    onsf = lambda s, p: ons + pot_ons(s, p) - p.mu_lead * np.eye(4)

    lead[lat.shape(lead_shape, (0, 0))] = onsf
    lead[kwant.HoppingKind((1, 0), lat)] = bhz_hopx(a)
    lead[kwant.HoppingKind((0, 1), lat)] = bhz_hopy(a)

    if finalized:
        return lead.finalized()
    else:
        return lead


def two_terminal(L, W, a):
    """Make two terminal system for bhz simulation."""
    def shape(pos):
        (x, y) = pos
        return (0 <= y < W and 0 <= x < L)

    lat = kwant.lattice.square()
    sys = kwant.Builder()

    ons = bhz_onsite(a)
    onsf = lambda s, p: ons + pot_ons(s, p) - p.mu * np.eye(4)

    sys[lat.shape(shape, (0, 0))] = onsf
    sys[kwant.HoppingKind((1, 0), lat)] = bhz_hopx(a)
    sys[kwant.HoppingKind((0, 1), lat)] = bhz_hopy(a)

    lead = make_lead(W, a, finalized=False)
    sys.attach_lead(lead)
    sys.attach_lead(lead.reversed())

    return sys.finalized()


# ********************** Important simulation code ***********************
@lru_cache()
def initialize(L, W, a, mu_lead):
    """Initialize precalculated two_terminal system.

    System is cached for fast computation.
    Leads have:
    * chemical_potential set to mu_lead
    * no magnetic field
    * no disorder
    * no edge potential
    * precalculated at energy=0

    This is achieved by using default parameters
    provided by ``define_parameters`` function.
    """
    par = define_parameters(L=L, W=W, a=a, mu_lead=mu_lead)

    sys = two_terminal(L, W, a)
    sys = sys.precalculate(args=[par])
    return sys


def calculate_conductance(simulation_parameters):
    """Calculate conductance according to input parameters

    This is main function of a simulaton.
    simulation_parameters should be created with define_parameters function.
    """
    p = simulation_parameters
    sys = initialize(p.L, p.W, p.a, p.mu_lead)

    smatrix = kwant.smatrix(sys, args=[simulation_parameters])
    transmission = smatrix.transmission(1, 0)

    output = dict(simulation_parameters.__dict__)
    output.update({'G': transmission})
    return output
