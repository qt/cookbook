#! /home/rskolasinski/miniconda3/envs/py35/bin/python
import feedparser
import re
from subprocess import call
import smtplib
from email.mime.text import MIMEText

# Words we only care about if they appear in the title
titlelist = []

# This is the word list we will care about for title or abstract
wordlist = ["majorana",
            "quantum computing"]

titlelist = titlelist + wordlist

# Here one inputs the authors
authors = ["Varjas"]

# Words entered all lowercase will be matched case-insensitively,
# words with any capitalisation will be matched with exact same capitalisation

# URL of the rss feed
feed = feedparser.parse('http://arxiv.org/rss/cond-mat')

# Email options
fromaddr = 'user1@server1.com'
toaddrs  = 'user2@server2.com'

# Credentials
username = 'user1'
password = 'pw1'

def passes_filter(entry): 
    return (any([word in entry.summary.lower() for word in wordlist])
            or any([word in entry.summary for word in wordlist])
	        or any([author in entry.author.lower() for author in authors])
            or any([author in entry.author for author in authors])
            or any([titleword in entry.title.lower() for titleword in titlelist])
            or any([titleword in entry.title for titleword in titlelist]))

def strip_html(text):
    return re.sub('<[^<]+?>', '', text)


filtered_entries = [entry for entry in feed.entries if passes_filter(entry)]

s = "--------------------Results--------------------\n"

for entry in filtered_entries:
    s = s + entry.title + "\n\n"
    s = s + "\t" + entry.author + "\n\n"
    s = s + entry.description + "\n---------------------------------------------\n\n"

s = strip_html(s)

# print(s)

msg = MIMEText(s)
msg['Subject'] = 'Today\'s arXiv'
msg['From'] = fromaddr
msg['To'] = toaddrs
msg = msg.as_string()

# The actual mail send
server = smtplib.SMTP('smtp.server1.com:587')
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
