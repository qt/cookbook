Using singularity on hpc05
==========================

We build Singularity [research-docker](https://gitlab.kwant-project.org/qt/research-docker) containers in additon to Docker containers.
They can be used to replicate Jupyterhub environment on the cluster.
Images can be pulled using the `sregistry` utility.

Prerequisites
-------------
User needs to install `sregistry` on hpc05 with:
```bash
pip install sregistry[all]
```
or:
```bash
conda install --channel conda-forge sregistry
```

To configure it, we need to add several environment variables to `~/.bash_profile`:
```bash
export SREGISTRY_CLIENT="gitlab"
export SREGISTRY_GITLAB_BASE="https://gitlab.kwant-project.org/"
export SREGISTRY_GITLAB_TOKEN="<your token here>"
```
(token can be obtained at https://gitlab.com/profile/personal_access_tokens,
`read_api` scope should be sufficient).

Pulling the images
------------------
If everything is set up correctly, the following command will output a list of CI stages, for example:
```
$ sregistry search qt/research-docker
[client|gitlab] [database|sqlite:////home/sostroukh/.singularity/sregistry.db]
Artifact Browsers (you will need path and job id for pull)
1  job_id       browser
2  123921       https://gitlab.kwant-project.org//qt/research-docker/-/jobs/123921/artifacts/browse/build singularity image
3  123920       https://gitlab.kwant-project.org//qt/research-docker/-/jobs/123920/artifacts/browse/build image
...
```
We are interested in those, that are suffixed by `build singularity image`.
Singularity images are stored within their artifacts.

To pull the image, use (for the job ID 123921):
```bash
$ sregistry pull 123921,qt/research-docker
```
Now we can run singularity in this container:
```bash
$ singularity run $(sregistry get 123921,qt/research-docker)
```
This should give you a shell with the prepared research environment. For example, we can test Kwant:
```bash
$ python -c "import kwant; kwant.test()"
```
