"""
Script to monitor IPython parallel engines.
"""
# Copyright (c) Bas Nijholt
# Distributed under the terms of the Modified BSD License

from collections import defaultdict
from datetime import datetime
import psutil
import sys

from ipyparallel import Client
import pandas as pd
from tornado import ioloop, options
from tornado.log import app_log


start_time = datetime.utcnow()


def get_usage():
    import socket
    import psutil
    from datetime import datetime
    mem = psutil.virtual_memory().percent
    hn = socket.gethostname()
    cpu = psutil.cpu_percent()
    return (hn, cpu, mem, datetime.utcnow())


class EngineMonitor(object):
    """An object for monitoring IPython parallel engines."""

    def __init__(self, client, interval, fname=None):
        """Initialize monitor, with current time."""
        self.client = client
        self.interval = interval
        self.usage = d = defaultdict(dict)
        self.started_at = datetime.utcnow()
        self.fname = fname

    def update_state(self):
        """Check engine resource usage.
        """
        app_log.debug("Updating state")
        result = self.client[:].apply(get_usage).result()
        for hn, cpu, mem, time in result:
            self.usage[hn]['mem'] = mem
            self.usage[hn]['cpu'] = cpu
            if 'cpu_max' in self.usage[hn]:
                self.usage[hn]['cpu_max'] = max(cpu, self.usage[hn]['cpu_max'])
            else:
                self.usage[hn]['cpu_max'] = cpu

            if 'mem_max' in self.usage[hn]:
                self.usage[hn]['mem_max'] = max(mem, self.usage[hn]['mem_max'])
            else:
                self.usage[hn]['mem_max'] = mem

            self.usage[hn]['last_update'] = str(time)

        for i in sorted(self.usage.items()):
            app_log.info(i)
        print()

        if self.fname is not None:
            df = pd.DataFrame(self.usage)
            with open(self.fname, 'w') as f:
                f.write(str(df.transpose()))
                f.write('\n')

        if (datetime.utcnow() - self.started_at).seconds > 86400 * 30:
            # Kill this script if it is still running after 
            # 30 days (for some unknown reason.)
            sys.exit(1)


def main():
    """Start IO loop that checks the resource usage every 60 seconds."""
    options.define('interval', default=60,
                   help="Interval (in seconds) at which state should be checked.")
    options.define('profile', default='pbs',
                   help="""Profile name.""")
    options.define('fname', default=None,
                   help="""Filename of resource usage table.""")
    options.parse_command_line()
    loop = ioloop.IOLoop.current()
    monitor = EngineMonitor(Client(profile=options.options.profile),
                          options.options.interval, options.options.fname)

    ioloop.PeriodicCallback(
        monitor.update_state, options.options.interval * 1000).start()
    loop.start()

if __name__ == '__main__':
    print('Running')
    main()
