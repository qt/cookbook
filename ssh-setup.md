# Set-up `ssh` to `io` and `hpc05`

## Requirements
* You need an account on __linux-bastion-ex.tudelft.nl__ (if you don't have it, send an email to servicepunttnw@tudelft.nl asking for it, include your NetID and employee number).
* Ask Jos (J.M.Thijssen@tudelft.nl or room F.338) if he can forward your request to get access to the `hpc05`
* You need [`homebrew`](https://brew.sh/) (only for macOS)

### Step 0
Generate a `ssh` key if you don't have one yet. See [here](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/) how to do it.

### Step 1
Send an email to servicepunttnw@tudelft.nl asking for an account to ssh `<netid>@linux-bastion-ex.tudelft.nl`.
This is needed to ssh into `io`.

### Step 2
Add you public key (see it with `cat ~/.ssh/id_rsa.pub`) to the ``.ssh/authorized_keys`` file on ``io``.
Then ask Slava or Anton to execute:
```
ansible-playbook -i hosts jupyterhubs/quantumtinkerer/ssh.yml --ask-become-pass --ask-vault-pass
```
Now you should have ssh access to `io`.

## Step 3
Create add the servers to your ssh config with `nano ~/.ssh/config` (**change YOUR_NETID**):

```
Host tudelft
    HostName linux-bastion-ex.tudelft.nl
    User YOUR_NETID

Host hpc05
    HostName hpc05.tudelft.net
    User YOUR_NETID

Host io
    User tinkerer
    Proxycommand ssh -q jupyterhub-quantumtinkerer@tn2

Host tnw-tn2 tn2
    HostName tnw-tn2.tudelft.net
    ProxyCommand ssh -q tudelft nc  %h 22
    User ansible
```

### Step 4
Setup password-less `ssh`:

Do `brew install ssh-copy-id` (**only on macOS**)

From your own computer get access to, `tudelft`, `io`, and `hpc05`.

Get access to `tudelft`
```
ssh-copy-id tudelft
```
get access to `io`
```
ssh-copy-id io
```
and get access to `hpc05`
```
ssh-copy-id hpc05
```

Try to ssh into `io`, with `ssh io`.

Once this works you need a ssh key on `io` too in order to `ssh` into `hpc05`.

You can **either** create new keys on `io` and a config, or just copy your ssh
config and keys from your computer to `io` (easier):
```
scp -r ~/.ssh io:
```

Now you should be able to do: `ssh io` -> `ssh hpc05`.

Some ways of scheduling jobs, like `adaptive-scheduler`, require password-less `ssh` between the physical nodes of `hpc05`. To set this up, run the following two lines on `hpc05`:

```bash
# Populate known hosts
for i in $(seq -f "%02g" 1 48); do ssh-keyscan -H n05-$i >> ~/.ssh/known_hosts; done
# Public key auth
ssh-copy-id hpc05
```

After this you should be able to `ssh hpc05` -> `ssh n05-xx` (where xx is the physical node id) without having to confirm any keys. Note that it is only possible to `ssh` to nodes where you have an active job running. To see the physical node your job is running on use `qstat -n`.