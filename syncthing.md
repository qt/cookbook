# Tutorial on how to setup automatic sync between your computer and `io`
Estimated amount of work: 5 minutes.

## Requirements
* You need to be able to `ssh` into `io`.
* You need [`homebrew`](https://brew.sh/) (only for macOS)

## For MacOS users:
```
brew install syncthing
brew services start syncthing
```

## For Linux users:
Install `syncthing`
```
sudo apt-get update && sudo apt-get install -y apt-transport-https wget curl systemd && \
  curl -s https://syncthing.net/release-key.txt | sudo apt-key add - && \
  echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list && \
  sudo apt-get update && sudo apt-get install -y syncthing
```
To auto start `syncthing`:
```
sudo mkdir -p /etc/systemd/ && sudo mkdir -p /etc/systemd/user && \
    sudo wget -O /etc/systemd/user/syncthing.service https://raw.githubusercontent.com/syncthing/syncthing/master/etc/linux-systemd/user/syncthing.service && \
    systemctl --user enable syncthing.service && \
    systemctl --user start syncthing.service
```

## Connect your local syncthing to `io` (both Linux and MacOS)

visit http://localhost:8384/ to see your running instance (it will always be there)

Restart your server on `io` to get the new image with `syncthing` installed.

create a tunnel such that the `syncthing` interface at `io` will be at http://localhost:8385
```
ssh -NL 8385:localhost:8384 io
```

* Go to both http://localhost:8384 (your computer) and http://localhost:8385 (io)

* On the `io` page go to **Actions** (top left) -> **Show ID** and copy the ID.

* On the local page click on **+ Add Remote Device** and fill in both **Device ID** and **Device Name**  and check **Default Folder** (or add a different folder yourself).

* On the `io` page you will now be asked if you want to add the remote machine (__takes a minute to show up__) and (__another minute later__) if you want to sync the folder.

The very first time it takes a few minutes for the connection to be made, but once it's up, files are transferred in fractions of seconds!

# Bonus: set-up on hpc05
Get the latest release with:
(check for the latest versions at: https://github.com/syncthing/syncthing/releases)
```
mkdir -p ~/syncthing && \
    wget -qO- https://github.com/syncthing/syncthing/releases/download/v1.1.4/syncthing-linux-amd64-v1.1.4.tar.gz | tar xvz -C ~/syncthing/ && \
    mv ~/syncthing/syncthing-linux-amd64-v*/syncthing ~/syncthing/
```


And start with:
```
nohup ~/syncthing/syncthing > /dev/null 2>&1 &
```

Setup the syncronization up in the same way as with `io`, so make a tunnel etc.
