# Installing a `kwant` branch on `io`
Here is example of installing `kwant`'s' `stable` branch.

## prepare environment and download `kwant`
```
git clone https://gitlab.kwant-project.org/kwant/kwant.git && cd kwant
git checkout stable  # get the branch you want to install
conda config --system --add envs_dirs ~/.conda/envs  # to set the correct folder for the env because https://gitlab.kwant-project.org/qt/research-docker/issues/4
conda env remove --yes --name stable  # remove the old environment if it exists
CONDA_ALWAYS_COPY=true conda env create --file /environments/python3.yml --name stable
source activate stable
conda remove --yes kwant tinyarray
conda install --yes c-compiler cxx-compiler lapack mumps cython pytest pytest-cov
conda clean --all --yes
```

## install `tinyarray` `master`
```
pip install git+https://gitlab.kwant-project.org/kwant/tinyarray.git
```


## create `build.conf` file with
```
[mumps]
include_dirs = PREFIX/include
library_dirs = PREFIX/lib
libraries = zmumps mumps_common pord metis esmumps scotch scotcherr mpiseq gfortran openblas
extra_link_args = -Wl,-rpath=PREFIX/lib
```

and run 
```
sed -i -e "s:PREFIX:$CONDA_PREFIX:g" build.conf
```


## build and install kwant
```
pip install .
```
